<div class="row">
  <? require_once("common/policies/sidebar.php"); ?>

    <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Streamline Production Quotas</h4>
            <hr>
            Influence the GDP of your country by working your people to death.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            Develop growth with a 70% chance of success and  30% chance of no growth. Not available to Free Market economies.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <div class="row">
                    <div class="col-md-12">

                      <!-- <p><input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($100,000)" name="glf"></p> -->
                      <p>
                      <!-- <button type="button" class="btn btn-primary btn-sm btn-block" aria-hidden="true" data-toggle="modal" data-target="#glf">Select This Policy ($100,000)</button> -->
                      </p>
                      <div class="modal fade" id="glf" tabindex="-1" role="dialog" aria-labelledby="glf" aria-hidden="true">
                          <div class="modal-dialog">
                              <div class="modal-content">
                                  <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title" id="glf">Confirm</h4>
                                  </div>
                                  <div class="modal-body">
                                      <input type="submit" class="btn btn-danger btn-block" value="Really use this policy?" name="glf"/>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <p>
                        <!-- <span class="help-block">Select a number of times to run this policy.</span> -->
                        <div class="input-group input-group">
                          <span class="input-group-addon">Select This Policy ($100,000)</span>
                          <input type="number" class="form-control" placeholder="Enter a number of times to run this policy." name="x">
                        </div>
                      </p>

                    </div>
                  </div>
                </form>
             
             </p></small></div>

          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Tax Incentives!</h4>
            <hr>
            Invite rich Yankees to exploit your cheap labor and resources.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            Slight chance of increasing economic growth... but the rest could always be nationalized later. Not available to Eastern aligned countries.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($75,000)" name="foreign_investment">
                  </p>
                </form>
             
             </p></small></div>

          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Nationalize Foreign Investment</h4>
            <hr>
            Take from the rich Yankees what is rightfully yours.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            Seizure of all foreign investment is added directly to your funds, but alienates the US and moves your nation closer towards the left. Significantly reduces stability.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="nationalize_investment">
                  </p>
                </form>
             
             </p></small></div>

          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Privatize</h4>
            <hr>
            Sell off state assets to the highest bidder.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            A 300,000 to your funds, move you closer to the free market, and reduce your GDP by $15 million. Significantly reduces stability.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="privatize">
                  </p>
                </form>
             
             </p></small></div>

          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Construct Oil Well</h4>
            <hr>
            Extract black gold from your once beautiful country.

            </div>

            <div class="panel-footer"><small><p class="text-muted">

            Constructs one oil well, producing 1 Mbbl (one thousand barrels) per month.

                <form action="<? echo $_SERVER['PHP_SELF']?>?type=economic" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($500,000)" name="prospect">
                  </p>
                </form>
             
             </p></small></div>

          </div>
  </div>
</div>