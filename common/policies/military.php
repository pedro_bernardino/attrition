<div class="row">
  <? require_once("common/policies/sidebar.php"); ?>

    <div class="col-md-8">
          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Selective Service</h4>
            <hr>
            Forcefully draft a couple thousand of your countries best men.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
            Increase size of military at the cost of economic growth, reduction in training and depletion of manpower.
                <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=military" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (Free)" name="conscript">
                  </p>
                </form>            
             </p></small></div>
          </div>

          <div class="panel panel-default">
            <div class="panel-body">
            <h4>Train Your Conscripts</h4>
            <hr>
            Turn your half-rate peasant army into a mindless killing machine. Cost is relative to the size of your army.
            </div>
            <div class="panel-footer"><small><p class="text-muted">
            Cost is based on the current amount of troops in your military.

                <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=military" method="post">
                  <p>
                    <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy ($<?php echo $english_format_number = number_format($u_a[troops] * 3); ?>)" name="train">
                  </p>
                </form>
             </p></small></div>
          </div>

    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Buy Old Bombers</h4>
        <hr>
        Under the cover of cheap cargo planes, get yourself some third party bombers.
      </div>
      <div class="panel-footer"><small><p class="text-muted">
        Increases air force size. Chance you discover the bombers never arrive and reputation decreases moderately.
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=military" method="post">
          <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (500,000)" name="buy_old_bombers">
        </form>
      </p></small></div>
    </div>

    <div class="panel panel-default">
      <div class="panel-body">
        <h4>Purchase Surplus Equipment</h4>
        <hr>
        Improve your armies equipment with a few Kalashnikov rifles and flecktarn flak-jackets.
      </div>
      <div class="panel-footer"><small><p class="text-muted">
        Increases equipment level.
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>?type=military" method="post">
          <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Policy (200,000)" name="purchase_surplus">
        </form>
      </p></small></div>
    </div>

    </div>
  </div>