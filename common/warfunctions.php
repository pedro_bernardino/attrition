<?php
    require_once("models/config.php");
    require_once("db/link_mysql.php");
    require_once("db/pdo.php");
    require_once("basicfunctions.php");
    require_once("userdata.php");
    function getwarinfo($uid) {
    	global $link;
        global $u_a;
	    $result = mysql_query("SELECT * FROM attr_events WHERE (attacker_id='$uid' OR defender_id='$uid') AND (event_type='war') AND (at_war='1') LIMIT 1", $link);
        $warinfo = mysql_fetch_array($result, MYSQL_BOTH);
        // echo 'At war: '.$warinfo[at_war].' Attacker ID: '.$warinfo[attacker_id].' Defender ID: '.$warinfo[defender_id];
        $rows = mysql_num_rows($result);
	    if (!$result) {
	        die('Could not query:' . mysql_error());
	    } elseif($rows > 0 and $warinfo[at_war] == '1') {

        	// Get user info
            $defender = mysql_query("SELECT * FROM attr_users WHERE user_id='$warinfo[defender_id]'", $link);
            if (!$defender) {
                die('Could not query:' . mysql_error());
            } else {
            	$defender = mysql_fetch_array($defender);

            	// Get total wars
                $result = mysql_query("SELECT event_id FROM attr_events WHERE attacker_id='$defender[user_id]' or defender_id='$defender[user_id]' AND event_type='war'", $link);
                if (!$result) {
                    die('Could not query:' . mysql_error());
                } else {
                    $defender_total_wars = mysql_num_rows($result);
                }
            }
            $attacker = mysql_query("SELECT * FROM attr_users WHERE user_id='$warinfo[attacker_id]'", $link);
            if (!$attacker) {
                die('Could not query:' . mysql_error());
            } else {
            	$attacker = mysql_fetch_array($attacker);

            	// Get total wars
                $result = mysql_query("SELECT event_id FROM attr_events WHERE attacker_id='$attacker[user_id]' or defender_id='$attacker[user_id]' AND event_type='war'", $link);
                if (!$result) {
                    die('Could not query:' . mysql_error());
                } else {
                    $attacker_total_wars = mysql_num_rows($result);
                }
            }

            // Get sides
            if($warinfo[defender_id] == $uid) {
            	$side = 'defender';
            	$opponent = $attacker;
            }
            if($warinfo[attacker_id] == $uid) {
            	$side = 'attacker';
            	$opponent = $defender;
            }

	        echo '
	        	<div class="row">
                	<div class="col-md-12">
                		<center><h4 class="text-muted">This country is the '.$side.' in a war with <u><a class="text-primary" href="user.php?uid='.$opponent[user_id].'">'.stripcslashes(ucwords($opponent[country_name])).'.</a></u></h4></center>
                    	<hr>
                    </div>

                    <div class="col-md-6">
                    	<h4 class="text-danger" align="center">Attacker:</h4>
                    	<hr>
                        <ul class="media-list">
                        <li class="media">
                          <a class="pull-left" href="user.php?uid='.$attacker[user_id].'">
                            <img class="media-object" style="width: 64px;"';

                                  if($attacker[custom_leader] and $u_a[safe_mode] == 0) {
                                        echo 'style="width: 100%;" src="'.$attacker[custom_leader].'">';
                                  } else {
                                        echo 'style="width: 100%;" src="'.getleaderfile($attacker[country_leader]).'">';
                                  }
                          echo '</a>
                          <div class="media-body">
                            <h4 class="media-heading">
                                '.getcountryprefix($attacker[gov_type]).' <a class="text-danger" href="user.php?uid='.$attacker[user_id].'"><u>'.ucwords($attacker[country_name]).'.</u></a>
                            </h4>';
                                if($attacker[description] == null) {
                                  if($attacker[user_id] == $u_a[user_id]) {
                                    echo '<small>Welcome to Attrition. This is the default country description. To change your description visit the <a href="preferences.php">settings</a> page.</small>';
                                  } else {
                                    echo '<span class="text-muted"><small>This country\'s leader has not set their description yet. <em>The country might be an easy target for an attack.</em></small></span>';
                                  }
                                } else {
                                  echo '<span class="text-muted"><small>' . stripcslashes($attacker[description]) . '</small></span>';
                                }
                          echo '</div>
                        </li>
                        </ul>


                    </div>

                    <div class="col-md-6">
                    	<h4 class="text-success" align="center">Defender:</h4>
                    	<hr>

                        <ul class="media-list">
                        <li class="media">
                          <a class="pull-left" href="user.php?uid='.$defender[user_id].'">
                            <img class="media-object" style="width: 64px;"';

                                  if($defender[custom_leader] and $u_a[safe_mode] == 0) {
                                        echo 'style="width: 100%;" src="'.$defender[custom_leader].'">';
                                  } else {
                                        echo 'style="width: 100%;" src="'.getleaderfile($defender[country_leader]).'">';
                                  }
                          echo '</a>
                          <div class="media-body">
                            <h4 class="media-heading">
                                '.getcountryprefix($defender[gov_type]).' <a class="text-success" href="user.php?uid='.$defender[user_id].'"><u>'.ucwords($defender[country_name]).'.</u></a>
                            </h4>';
                                if($defender[description] == null) {
                                  if($defender[user_id] == $u_a[user_id]) {
                                    echo '<small>Welcome to Attrition. This is the default country description. To change your description visit the <a href="preferences.php">settings</a> page.</small>';
                                  } else {
                                    echo '<span class="text-muted"><small>This country\'s leader has not set their description yet. <em>The country might be an easy target for an attack.</em></small></span>';
                                  }
                                } else {
                                  echo '<span class="text-muted"><small>' . stripcslashes($defender[description]) . '</small></span>';
                                }
                          echo '</div>
                        </li>
                        </ul>

                    </div>

                </div>
                <div class="row">
                    <hr>
                	<div class="col-md-6">
                      	<table class="table table-hover table-striped">
                        	<tbody>
                              	<tr>
                                	<td><h6>Troops</h6></td>
                                	<td>
                                  		<h6 class="text-muted">'.$english_format_number = number_format($attacker[troops]).'</h6>
                                	</td>
                              	</tr>
                                <tr>
                                    <td><h6>Manpower</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.getmanpower($attacker[user_id]).'</h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h6>Training</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.gettraining($attacker[user_id]).'</h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h6>Air Force</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.getairforce($attacker[user_id]).'</h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h6>Equipment</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.getequipment($attacker[user_id]).'</h6>
                                    </td>
                                </tr>
                        	</tbody>
                      	</table>
                  	</div>

                  	<div class="col-md-6">
                      	<table class="table table-hover table-striped">
                        	<tbody>
                              	<tr>
                                	<td><h6>Troops</h6></td>
                                	<td>
                                  		<h6 class="text-muted">'.$english_format_number = number_format($defender[troops]).'</h6>
                                	</td>
                              	</tr>
                                <tr>
                                    <td><h6>Manpower</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.getmanpower($defender[user_id]).'</h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h6>Training</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.gettraining($defender[user_id]).'</h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h6>Air Force</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.getairforce($defender[user_id]).'</h6>
                                    </td>
                                </tr>
                                <tr>
                                    <td><h6>Equipment</h6></td>
                                    <td>
                                        <h6 class="text-muted">'.getequipment($defender[user_id]).'</h6>
                                    </td>
                                </tr>
                        	</tbody>
                      	</table>
                    </div>
                </div>
                <hr>
            ';
	    }
    }
    function getwaroptions($user, $uid) {
        require_once("uiddata.php");
        global $pdo;
	   	global $link;
        global $u_a;
        global $uid_a;

        // echo '$user: '.$user.' $uid: '.$uid;

	    $result = mysql_query("SELECT * FROM attr_events WHERE (event_type='war') AND (at_war='1') AND (attacker_id='$uid' OR defender_id='$uid') LIMIT 1", $link);
	    if (!$result) {
	        die('Could not query:' . mysql_error());
	    } else {
	    	$waroptions = mysql_fetch_assoc($result);
	        $rows = mysql_num_rows($result);
            // echo '<br>Rows: '.$rows.' Attacker ID: '.$waroptions[attacker_id].' Defender ID: '.$waroptions[defender_id].' At war: '.$waroptions[at_war];
            // echo '<br>';
            // print_r($waroptions);
	    }
	    if($user != $uid && $waroptions[at_war] == '1' && $user == $waroptions[attacker_id] || $user == $waroptions[defender_id] AND $uid == $waroptions[attacker_id] || $uid == $waroptions[defender_id]) {
	    	echo '
                <center><h4 class="text-muted">War Operations</h4></center>
                <hr>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <h4>Infantry Offensive</h4>
                        <hr>

                        Attack the country in a conventional way with the brute strength of your infantry.
                    </div>

                    <div class="panel-footer"><small><p class="text-muted">
                        You must have at least 5,000 troops to attack. Their country will fall and the war will be won if the country has less than 5,000 troops.
                        <form action="'.$_SERVER['PHP_SELF'].'?uid='.$uid_a[user_id].'" method="post">
                            <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Option" name="offensive">
                        </form>
                    </p></small></div>
                </div>

                <hr><div class="panel panel-default">
                    <div class="panel-body">
                        <h4>Bombing Campaign</h4>
                        <hr>

                        Devastate the opposition with superior air power.
                    </div>

                    <div class="panel-footer"><small><p class="text-muted">
                        Must have an air force and at least 5 Mbbls of oil. Attack structural and economic targets to weaken the opposing country.
                        <form action="'.$_SERVER['PHP_SELF'].'?uid='.$uid_a[user_id].'" method="post">
                            <input type="submit" class="btn btn-primary btn-sm btn-block" value="Select This Option" name="bombing_campaign">
                        </form>
                    </p></small></div>
                </div>
            ';

                if($user != $uid && ($waroptions[at_war] == '1')) {
                    echo '
                        <form action="'.$_SERVER['PHP_SELF'].'?uid='.$uid_a[user_id].'" method="post">
                        <button class="btn btn-success btn-block" data-toggle="modal" data-target="#appeal_peace">
                            Appeal for Peace
                        </button>
                        <div class="modal fade" id="appeal_peace" tabindex="-1" role="dialog" aria-labelledby="appeal_peace" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="appeal_peace">Confirm</h4>
                              </div>
                              <div class="modal-body">
                                <input type="submit" class="btn btn-success btn-block" value="Appeal for Peace" name="appeal_peace">
                              </div>
                            </div>
                          </div>
                        </div>
                        </form>
        	    	';
                }
	    } elseif($user != $uid && ($waroptions[at_war] == '0' or $rows < 1)) {
            echo '
                <form action="'.$_SERVER['PHP_SELF'].'?uid='.$uid_a[user_id].'" method="post">
                    <button class="btn btn-danger btn-block" data-toggle="modal" data-target="#declare_war">
                        Declare War on '.stripcslashes(ucwords($uid_a[country_name])).'.
                    </button>
                    <div class="modal fade" id="declare_war" tabindex="-1" role="dialog" aria-labelledby="declare_war" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="declare_war">Confirm</h4>
                          </div>
                          <div class="modal-body">
                            <input type="submit" class="btn btn-danger btn-block" value="Declare War on '.stripcslashes(ucwords($uid_a[country_name])).'." name="start_war">
                          </div>
                        </div>
                      </div>
                    </div>
                </form>
            ';
        }
        if($user == $uid) {
            echo '<input type="submit" class="btn btn-danger btn-block disabled" value="You cannot declare war on yourself." name="start_war">';
        }
        // if($user != $uid) {
        //     echo '
        //         <form action="'.$_SERVER['PHP_SELF'].'?uid='.$uid_a[user_id].'" method="post">
        //             <input type="submit" class="btn btn-danger btn-block" value="Supply '.stripcslashes(ucwords($uid_a[country_name])).' with 10,000 troops." name="send_troops">
        //         </form>
        //     ';
        // }
    }
?>