<div class="row">
    <? require_once("common/alliances/sidebar.php"); ?>

    <div class="col-md-8">

        <div class="panel panel-primary">

            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="icon-globe"></i> Alliance List
                </h3>
            </div>

            <div class="panel-body">

                <?php

                    $limit = 10;

                    if(isset($_GET["page"]) and $_GET["page"] == 0 ) {

                        $page  = 1;

                    } elseif(isset($_GET["page"])) {

                        $page  = $_GET["page"];

                    } else {

                        $page=1;

                    };  

                    $start_from = ($page-1) * $limit;  

                    // Retrieve data
                    $result = mysql_query("SELECT * FROM {$dbprefix}alliances ORDER BY members DESC LIMIT $start_from, $limit", $link);

                    $row = mysql_fetch_assoc($result);
                    $num_rows = mysql_num_rows($result);

                    // Display data
                    if($num_rows > 0) { ?>

                    <table class="table table-hover table-striped">

                        <thead>
                            <tr>
                                <th class="text-muted">AID</th>
                                <th class="text-muted">Alliance Name</th>
                                <th class="text-muted">Total Members</th>
                                <th class="text-muted">Troops</th>
                                <th class="text-muted">Est Date</th>
                            </tr>
                        </thead>

                        <tbody>

                        <? do { ?>

                            <tr>
                                <td>
                                    <h5><a href="alliance.php?aid=<? echo $row[alliance_id]; ?>" class="text-muted"><? echo $row[alliance_id] ?></a></h5>
                                </td>
                                <td>
                                    <h5><a href="alliance.php?aid=<? echo $row[alliance_id]; ?>"><? echo stripcslashes(ucwords($row[alliance_name])) ?></a></h5>
                                </td>
                                <td>
                                    <h5 class="text-muted">
                                        <? 

                                            // Calculate and set the number of members
                                            $result2 = mysql_query("SELECT alliance_id FROM {$dbprefix}users WHERE alliance_id='$row[alliance_id]'", $link);
                                            if (!$result2) {
                                                die('Could not query:' . mysql_error());
                                            } else {
                                                $rows = mysql_num_rows($result2);
                                                // Set the number of members
                                                $sql = ("UPDATE {$dbprefix}alliances SET members = :members WHERE alliance_id='$row[alliance_id]'");
                                                $stmt = $pdo->prepare($sql);
                                                $stmt->bindParam(':members', $rows, PDO::PARAM_STR);
                                                $stmt->execute();
                                            }

                                        echo $row[members]; ?>
                                    </h5>
                                </td>
                                <td>
                                    <h5 class="text-muted">
                                    <?
                                        if($row[troop_bank] > 0) {
                                            echo number_format($row[troop_bank]);
                                        } else {
                                            echo 'None';
                                        }
                                    ?>
                                    </h5>
                                </td>
                                <td>
                                    <h5 class="text-muted"><? echo date('m/d/y',strtotime($row[est_date])) ?></h5>
                                </td>
                            </tr>

                        <? } while ($row = mysql_fetch_assoc($result));

                    }

                    ?>

                    </tbody>
                </table>

                <center>

                    <?php  

                        $result = mysql_query("SELECT COUNT(*) FROM {$dbprefix}alliances", $link);
                        $row = mysql_fetch_row($result);

                        $total_records = $row[0];  
                        $total_pages = ceil($total_records / $limit); 
                        $pagLink = '<ul class="pagination">';

                        for ($i=1; $i<=$total_pages; $i++) {  
                                     $pagLink .= "<li><a href='alliances?tab=list&page=".$i."''>".$i."</a></li>";  
                        };

                        echo $pagLink . '</ul>';  
                    ?>
                </center>

            </div>
        </div>
    </div>
</div>