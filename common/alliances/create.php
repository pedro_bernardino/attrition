<div class="row">
    <? require_once("common/alliances/sidebar.php"); ?>

    <div class="col-md-8">

       <div class="panel panel-info">

            <div class="panel-heading">
                <h3 class="panel-title">
                    <i class="icon-globe"></i> Alliance Declaration
                </h3>
            </div>

            <div class="panel-body">

                    <?php

                        if(!isUserLoggedIn()) {
                            
                            echo 'You must be logged in to create an alliance.';

                        } elseif(!$u_a[alliance_id]) { ?>

                            <form action="<?php echo $_SERVER['PHP_SELF'] ?>?tab=list" method="post">
                                
                                <p>
                                	<input type="text" class="form-control" placeholder="Enter an alliance name." name="alliance_name" maxlength="20">
                                </p>

                                <p>
                                    <select class="form-control" name="alliance_flag" class="form-control">
                                    <option value="" disabled selected style="display:none;">Choose an alliance flag.</option>
                                    <option value="afghanistan">Afghanistan</option>
                                    <option value="albania">Albania</option>
                                    <option value="arstotzka">Arstotzka</option>
                                    <option value="australia">Australia</option>
                                    <option value="bosnia">Bosnia</option>
                                    <option value="bulgaria">Bulgaria</option>
                                    <option value="china">Chinese Flag</option>
                                    <option value="confederate_first">First Confederate Navy Jack</option>
                                    <option value="confederate">Second Confederate Navy Jack</option>
                                    <option value="egypt">Egypt</option>
                                    <option value="germany">Germany</option>
                                    <option value="ghana">Ghana</option>
                                    <option value="happening">Happening</option>
                                    <option value="indianbloc">All India Forward Bloc Flag</option>
                                    <option value="iran">Iran</option>
                                    <option value="israel">Israel</option>
                                    <option value="libya">Libya</option>
                                    <option value="nk">North Korean Flag</option>
                                    <option value="pirate">Pirate Flag</option>
                                    <option value="rhodesia">Rhodesian Flag</option>
                                    <option value="russia">Russian Flag</option>
                                    <option value="saudi_arabia">Saudi Arabia</option>
                                    <option value="soviet_union">Soviet Union Flag</option>
                                    <option value="swastika">Nazi Germany Flag</option>
                                    <option value="syria">Syria</option>
                                    <option value="taliban">Taliban</option>
                                    <option value="tread">Gadsden Flag</option>
                                    <option value="un">UN Flag</option>
                                    <option value="us">American Flag</option>
                                    </select>
                                </p>

                                <p>
                                	<input type="submit" class="btn btn-primary btn-block" value="Create an alliance for $100,000 funds." name="alliance_new"/>
                                </p>

                            </form>

                        <? }

                        if($u_a[alliance_id]) {

                            echo 'You are already in the alliance: <a href="alliance?uid=' . $a_a[alliance_id] . '">'.$a_a[alliance_name].'</a>';

                        }

                    ?>
              
            </div>
        </div>

    </div>
</div>