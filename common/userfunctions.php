<?
    function country_namecheck($country_name) {
        global $pdo, $dbprefix, $errors;
        $stmt = $pdo->prepare("SELECT * FROM {$dbprefix}users WHERE country_name='$country_name'");
        $stmt->execute();

        if($stmt->rowCount() > 0) {
            $errors[] = 'That country name is already in use.';
        }

        $reserved_countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

        if (in_array($country_name, $reserved_countries)) {
            $errors[] = 'That country name is reserved.';
        }
    }
    if (!empty($_POST['pm'])) {
        $pm = trim($_POST["private_message"]);

        if(!$pm) {
            $errors[] = 'You must enter a message.';
        }
        if(count($errors) == 0) {
            $debug_good[] = 'private_message successfully validated.';
            $sql = ("INSERT INTO {$dbprefix}mail (user_id, sender_id, mail_type, title, string) VALUES(:user_id, :sender_id, :mail_type, :title, :string)");
            $stmt = $pdo->prepare($sql);

            $mail_type = 'pm';
            $title = getcountryprefix($u_a[gov_type]).' <a href="user.php?uid='.$u_a[user_id].'"><u>'.ucwords($u_a[country_name]).'</u></a> has sent you a private message.';

            $stmt->bindParam(':user_id', $uid_a[user_id], PDO::PARAM_INT);
            $stmt->bindParam(':sender_id', $u_a[user_id], PDO::PARAM_INT);
            $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR);
            $stmt->bindParam(':title', $title, PDO::PARAM_STR);
            $stmt->bindParam(':string', $pm, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->rowCount() > 0) {
                $debug_good[] = 'private_message message successfully processed.';
                $outcome_good[] = 'The private message has been successfully sent.';
            } else {
                $debug_bad[] = 'No private_message data entered.';
                $errors[] = 'private_message creation failed. (No data entered)';
            }
        }
    }
    if(isset($_POST['start_war'])) {
        $debug_good[] = 'Event setup started.';
        if ($u_a[user_id] == $uid_a[user_id]) {
            $debug_info[] = 'Attempted suicide.';
            $errors[] = '"Never go to war. Especially with yourself." - Yuri Orlov';
        }
        $debug_info[] = 'Attacker ID: '.$uid;
        $result = mysql_query("SELECT * FROM {$dbprefix}events WHERE attacker_id='{$u_a[user_id]}' and at_war='1'", $link);
        if (!$result) {
    	  die('Could not query:' . mysql_error());
        } else {
    	    if(mysql_num_rows($result)) {
                $errors[] = 'Attack only one nation at a time! Didn\'t you learn anything from Hitler?';
            }
        }
        $result = mysql_query("SELECT * FROM {$dbprefix}events WHERE at_war='1' and defender_id='{$uid_a[user_id]}' or attacker_id='{$uid_a[user_id]}' and at_war='1'", $link);
        if (!$result) {
            die('Could not query:' . mysql_error());
        } else {
            if (mysql_num_rows($result)) {
                $errors[] = 'You are unable to start a war with someone that is already in a war.';
            }
    	}
        if($uid_a[gdp] < ($u_a[gdp] * 0.75)) {
            $debug_bad[] = 'Defender\'s GDP is too low.';
            $errors[] = 'You cannot attack a country with less than 75% of your GDP.';
        } else {
            $debug_info[] = 'Defender\'s GDP is greater than 75% of Attacker\'s GDP.'; 
        }
        if($uid_a[gdp] > ($u_a[gdp] * 2)) {
            $debug_bad[] = 'Defender\'s GDP is too high.';
            $errors[] = 'You cannot attack a country with a GDP that is at least 200% of yours.';
        } else {
            $debug_info[] = 'Defender\'s GDP is less than 200% of Attacker\'s GDP.'; 
        }

        $datetime1 = new DateTime(date('o-m-d ' ,$uid_a[sign_up_date]));
        $datetime2 = new DateTime();
        $interval = $datetime1->diff($datetime2);
        if($interval->format('%a') <= 1) {
            $errors[] = 'The country must be more than a day old to attack.';
        }

        if(count($errors) == 0) {
            $debug_good[] = 'Both countries are eligible for war.';

            $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id,defender_id,at_war) VALUES (:event_type,:event_action,:attacker_id,:defender_id,:at_war)";
            $stmt = $pdo->prepare($sql);

            // Config
            $event_type = 'war';
            $event_action = 'declared war against';
            $at_war = 1;

            $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
            $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
            $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
            $stmt->bindParam(':defender_id', $uid_a[user_id], PDO::PARAM_INT);
            $stmt->bindParam(':at_war', $at_war, PDO::PARAM_INT);

            $stmt->execute();

            if($stmt->rowCount() > 0) {

                $debug_good[] = 'War data successfully processed.';
                $outcome_bad[] = 'You are now at war!';

                // Mail Query - Defender
                $sql = ("INSERT INTO {$dbprefix}mail (user_id, mail_type, mail_subtype, title, string) VALUES (:user_id, :mail_type, :mail_subtype, :title, :string)");
                $stmt = $pdo->prepare($sql);

                $mail_type = 'event';
                $mail_subtype = 'war';
                $title = getcountryprefix($u_a[gov_type]).' <a class="text-danger" href="user.php?uid='.$u_a[user_id].'"><u>'.ucwords($u_a[country_name]).'</u></a> has declared war on you.';
                $string = 'Check your opponent\'s profile to counter-attack.';

                $stmt->bindParam(':user_id', $uid_a[user_id], PDO::PARAM_INT);
                $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR); 
                $stmt->bindParam(':mail_subtype', $mail_subtype, PDO::PARAM_STR); 
                $stmt->bindParam(':title', $title, PDO::PARAM_STR);
                $stmt->bindParam(':string', $string, PDO::PARAM_STR);
                $stmt->execute();

                if($stmt->rowCount() <= 0) {
                    $debug_bad[] = 'No defender mail data entered.';
                    $errors[] = 'Mail creation failed. (No data entered)';
                }
            } else {
                $debug_bad[] = 'No event data entered.';
                $errors[] = 'War creation failed. (No data entered)';
            }
        } else {
            $debug_bad[] = 'At lease one country is not eligible for war.';
        }
    }
    if(isset($_POST['appeal_peace'])) {
        $result = mysql_query("SELECT * FROM attr_events WHERE (event_type='war') AND (at_war='1') AND (attacker_id='$u_a[user_id]' OR defender_id='$u_a[user_id]') AND (attacker_id='$uid_a[user_id]' OR defender_id='$uid_a[user_id]') LIMIT 1", $link);
        if (!$result) {
            die('Could not query:' . mysql_error());
        } else {
            $waroptions = mysql_fetch_assoc($result);
            $rows = mysql_num_rows($result);
            // echo '<br>Rows: '.$rows.' Attacker ID: '.$waroptions[attacker_id].' Defender ID: '.$waroptions[defender_id].' At war: '.$waroptions[at_war];
            // echo '<br>';
            // print_r($waroptions);
        }
        if($u_a[user_id] == $uid_a[user_id]) {
            $errors[] = 'You cannot request peace with yourself.';
        }
        if($waroptions[at_war] == '0') {
            $errors[] = 'You are not at war.';
        }
        if(!$errors) {
            $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE (user_id='$u_a[user_id]') AND (sender_id='$uid_a[user_id]') AND (mail_type='event') AND (mail_subtype='appeal_peace') LIMIT 1", $link);
            if (!$result) {
                die('Could not query:' . mysql_error());
            } else {
                $sql_a = mysql_fetch_array($result, MYSQL_BOTH);
                $rows = mysql_num_rows($result);

                if($rows > 0) {
                    // Update the start war event
                    $sql = ("UPDATE {$dbprefix}events SET at_war = :at_war WHERE event_type='war' AND at_war='1' AND attacker_id='$u_a[user_id]' OR defender_id='$u_a[user_id]' AND attacker_id='$uid_a[user_id]' OR defender_id='$uid_a[user_id]'");
                    $stmt = $pdo->prepare($sql);

                    // Config
                    $at_war = 0;
                    $stmt->bindParam(':at_war', $at_war, PDO::PARAM_INT);
                    $stmt->execute();

                    // Create war over event
                    $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id,defender_id) VALUES (:event_type,:event_action,:attacker_id,:defender_id)";
                    $stmt = $pdo->prepare($sql);

                    // Config
                    $event_type = 'war';
                    $event_action = 'accepted peace with';

                    $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
                    $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
                    $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
                    $stmt->bindParam(':defender_id', $uid_a[user_id], PDO::PARAM_INT);
                    $stmt->execute();

                    if($stmt->rowCount() > 0) {
                          $sql = "DELETE FROM {$dbprefix}mail WHERE (user_id='$u_a[user_id]') AND (mail_type='event') AND (mail_subtype='appeal_peace') LIMIT 1";
                          $stmt = $pdo->prepare($sql);

                          // Bind
                          $stmt->bindParam(':user_id', $u_a[user_id], PDO::PARAM_INT);

                          // Continue without errors
                          if(count($errors) == 0) { 
                            $stmt->execute();
                          }

                        // Mail Query - Defender
                        $sql = ("INSERT INTO {$dbprefix}mail (user_id, mail_type, mail_subtype, title, string) VALUES (:user_id, :mail_type, :mail_subtype, :title, :string)");
                        $stmt = $pdo->prepare($sql);

                        // Config
                        $mail_type = 'event';
                        $mail_subtype = 'appeal_peace';
                        $title = '<a class="text-success" href="user.php?uid='.$u_a[user_id].'"><u>'.ucwords($u_a[country_name]).'</u></a> has accepted your request for peace.';
                        $string = 'Now kiss and make up.';

                        // Bind
                        $stmt->bindParam(':user_id', $uid_a[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR);
                        $stmt->bindParam(':mail_subtype', $mail_subtype, PDO::PARAM_STR);
                        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
                        $stmt->bindParam(':string', $string, PDO::PARAM_STR);
                        $stmt->execute();
                        $outcome_good[] = 'Ceasefire accepted. Now kiss and make up.';
                    }
                } elseif(!$errors) {
                    $result = mysql_query("SELECT * FROM {$dbprefix}mail WHERE (user_id='$uid_a[user_id]') AND (sender_id='$u_a[user_id]') AND (mail_type='event') AND (mail_subtype='appeal_peace') LIMIT 1", $link);
                    $rows = mysql_num_rows($result);
                    $sql_a = mysql_fetch_array($result, MYSQL_BOTH);

                    if($sql_a[sender_id] == $u_a[user_id]) {
                        $errors[] = 'You have already requested for peace.';
                    }
                    if(!$errors) {
                        // create the appeal
                        $sql = ("INSERT INTO {$dbprefix}mail (user_id, sender_id, mail_type, mail_subtype, title, string) VALUES (:user_id, :sender_id, :mail_type, :mail_subtype, :title, :string)");
                        $stmt = $pdo->prepare($sql);

                        $mail_type = 'event';
                        $mail_subtype = 'appeal_peace';
                        $title = '<a class="text-danger" href="user.php?uid='.$u_a[user_id].'"><u>'.ucwords($u_a[country_name]).'</u></a> has requested to end the war.';
                        $string = 'Visit their profile and appeal for peace to accept.';

                        $stmt->bindParam(':user_id', $uid_a[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':sender_id', $u_a[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR); 
                        $stmt->bindParam(':mail_subtype', $mail_subtype, PDO::PARAM_STR); 
                        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
                        $stmt->bindParam(':string', $string, PDO::PARAM_STR);
                        $stmt->execute();
                        $outcome_good[] = 'Ceasefire successfully requested.';

                        $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id,defender_id) VALUES (:event_type,:event_action,:attacker_id,:defender_id)";
                        $stmt = $pdo->prepare($sql);

                        // Config
                        $event_type = 'war';
                        $event_action = 'requested peace with';

                        $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
                        $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
                        $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':defender_id', $uid_a[user_id], PDO::PARAM_INT);
                        $stmt->execute();
                    }
                }
            }
        }
    }
    if(isset($_POST['offensive'])) {
        if ($u_a[user_id] == $uid_a[user_id]) {
            $debug_info[] = 'Attempted suicide.';
            $errors[] = '"Never go to war. Especially with yourself." - Yuri Orlov';
        }
        if($u_a[troops] < 5000) {
            $errors[] = 'You must have more than 5,000 troops to launch an offensive.';
        }
        if($u_a[has_attacked] > 0) {
            $errors[] = 'You can only attack once per turn.';
        }
        if(!$errors) {
            // Pre config
            $attacker_gdp = $u_a[gdp];
            $defender_gdp = $uid_a[gdp];
            $attacker_funds = $u_a[funds];
            $defender_funds = $uid_a[funds];

            $favor = 0;
            if($uid_a[troops] < 5000) {
                $outcome_good[] = 'You have won the war! You receive 20% of the country\'s GDP and funds.';
                $attacker_troops = $u_a[troops];
                $defender_troops = $uid_a[troops] * 0.50;
                $attacker_gdp = $u_a[gdp] + ($uid_a[gdp] * 0.20);
                $defender_gdp = $uid_a[gdp] * 0.80;
                $attacker_funds = $u_a[funds] + ($uid_a[funds] * 0.20);
                $defender_funds = $uid_a[funds] * 0.80;
            }
            if($u_a[tech] >= $uid_a[tech]) {
                $favor = $favor + 1;
            }
            if($u_a[troops] >= $uid_a[troops]) {
                $favor = $favor + 1;
            }
            if($u_a[training] >= $uid_a[training]) {
                $favor = $favor + 1;
            }
            if($favor == 3) {
                $outcome_good[] = 'Major victory. Your men return home as heroes.';
                $attacker_troops = $u_a[troops] * 0.95;
                $defender_troops = $uid_a[troops] * 0.50;
                $defender_mail[result] = 'major loss';
            } elseif($favor == 2) {
                $outcome_good[] = 'Minor victory. Your men win the battle, but suffer some casualties.';
                $attacker_troops = $u_a[troops] * 0.85;
                $defender_troops = $uid_a[troops] * 0.80;
                $defender_mail[result] = 'minor loss';
            } elseif($favor == 1) {
                $outcome_bad[] = 'Minor defeat. You lose the battle, and suffer some casualties.';
                $attacker_troops = $u_a[troops] * 0.80;
                $defender_troops = $uid_a[troops] * 0.85;
                $defender_mail[result] = 'minor victory';
            } elseif($favor <= 0) {
                $outcome_bad[] = 'Major defeat. Your army is severely beaten, your men turn and hang their heads in defeat.';
                $attacker_troops = $u_a[troops] * 0.50;
                $defender_troops = $uid_a[troops] * 0.95;
                $defender_mail[result] = 'major victory';
            }
                $defender_mail[troop_loss] = $defender_troops;
                $defender_mail[troop_total] = $uid_a[troops];

            $sql = ("UPDATE {$dbprefix}users SET has_attacked = :has_attacked, troops = :troops, funds = :funds, gdp = :gdp WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            // Config
            $has_attacked = true;

            $stmt->bindParam(':has_attacked', $has_attacked, PDO::PARAM_INT);
            $stmt->bindParam(':troops', $attacker_troops, PDO::PARAM_INT);
            $stmt->bindParam(':funds', $attacker_funds, PDO::PARAM_INT);
            $stmt->bindParam(':gdp', $attacker_gdp, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->rowCount() > 0) {
                $sql = ("UPDATE {$dbprefix}users SET troops = :troops, funds = :funds, gdp = :gdp WHERE user_id='$uid_a[user_id]'");
                $stmt = $pdo->prepare($sql);

                $stmt->bindParam(':troops', $defender_troops, PDO::PARAM_INT);
                $stmt->bindParam(':funds', $defender_funds, PDO::PARAM_INT);
                $stmt->bindParam(':gdp', $defender_gdp, PDO::PARAM_INT);
                $stmt->execute();

                if($stmt->rowCount() > 0) {
                    $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id,defender_id) VALUES (:event_type,:event_action,:attacker_id,:defender_id)";
                    $stmt = $pdo->prepare($sql);

                    // Config
                    $event_type = 'battle';
                    $event_action = 'launched an offensive against';

                    $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
                    $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
                    $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
                    $stmt->bindParam(':defender_id', $uid_a[user_id], PDO::PARAM_INT);
                    $stmt->execute();

                    if($stmt->rowCount() > 0 and $uid_a[troops] >= 5000) {
                        // Mail Query - Defender
                        $sql = ("INSERT INTO {$dbprefix}mail (user_id, mail_type, mail_subtype, title, string) VALUES (:user_id, :mail_type, :mail_subtype, :title, :string)");
                        $stmt = $pdo->prepare($sql);

                        // Config
                        $mail_type = 'event';
                        $mail_subtype = 'war';
                        $title = '<a class="text-danger" href="user.php?uid='.$u_a[user_id].'"><u>'.ucwords($u_a[country_name]).'</u></a> has launched an offensive against your country.';
                        $string = 'The battle resulted in a '.$defender_mail[result].' for your country.<br>Your country has lost <u>'.number_format($defender_mail[troop_loss]).'</u> of <u>'.number_format($defender_mail[troop_total]).'</u> total troops.';

                        // Bind
                        $stmt->bindParam(':user_id', $uid_a[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR);
                        $stmt->bindParam(':mail_subtype', $mail_subtype, PDO::PARAM_STR);
                        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
                        $stmt->bindParam(':string', $string, PDO::PARAM_STR);
                        $stmt->execute();
                    }

                    if($uid_a[troops] < 5000) {
                        // Update the start war event
                        $sql = ("UPDATE {$dbprefix}events SET at_war = :at_war WHERE event_type='war' AND at_war='1' AND attacker_id='$u_a[user_id]' OR defender_id='$u_a[user_id]' AND attacker_id='$uid_a[user_id]' OR defender_id='$uid_a[user_id]'");
                        $stmt = $pdo->prepare($sql);

                        // Config
                        $at_war = 0;
                        $stmt->bindParam(':at_war', $at_war, PDO::PARAM_INT);
                        $stmt->execute();

                        // Create war over event
                        $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id,defender_id) VALUES (:event_type,:event_action,:attacker_id,:defender_id)";
                        $stmt = $pdo->prepare($sql);

                        // Config
                        $event_type = 'defeated';
                        $event_action = 'defeated';

                        $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
                        $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
                        $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
                        $stmt->bindParam(':defender_id', $uid_a[user_id], PDO::PARAM_INT);
                        $stmt->execute();

                        if($stmt->rowCount() > 0) {
                            // Mail Query - Defender
                            $sql = ("INSERT INTO {$dbprefix}mail (user_id, mail_type, mail_subtype, title, string) VALUES (:user_id, :mail_type, :mail_subtype, :title, :string)");
                            $stmt = $pdo->prepare($sql);

                            // Config
                            $mail_type = 'event';
                            $mail_subtype = 'war';
                            $title = 'You have been defeated in war with <a class="text-danger" href="user.php?uid='.$u_a[user_id].'"><u>'.ucwords($u_a[country_name]).'.</u></a>';
                            $string = 'You have lost 20% of your funds and GDP.';

                            // Bind
                            $stmt->bindParam(':user_id', $uid_a[user_id], PDO::PARAM_INT);
                            $stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR);
                            $stmt->bindParam(':mail_subtype', $mail_subtype, PDO::PARAM_STR);
                            $stmt->bindParam(':title', $title, PDO::PARAM_STR);
                            $stmt->bindParam(':string', $string, PDO::PARAM_STR);
                            $stmt->execute();
                        }

                        if($stmt->rowCount() == 0) {
                            $debug_bad[] = 'No event info data entered.';
                            $errors[] = 'Unable to event info. (Report this as a bug)';
                        }
                    }
                } else {
                    $debug_bad[] = 'No uid info data entered.';
                    $errors[] = 'Unable to update uid info. (Report this as a bug)';
                }
            } else {
                $debug_bad[] = 'No u_a info data entered.';
                $errors[] = 'Unable to update u_a info. (Report this as a bug)';
            }
        }
    }
    if(isset($_POST['send_troops'])) {
        if($u_a[troops] < 10000) {
            $errors[] = 'You must have at least 10,000 troops to send.';
        }
        if($u_a[recieved_troops] > 0) {
            $errors[] = 'This country has already received troops this turn.';
        }
        if(!$errors) {
            $sql = ("UPDATE {$dbprefix}users SET troops = :troops WHERE user_id='$u_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $troops = $u_a[troops] - 10000;

            $stmt->bindParam(':troops', $troops, PDO::PARAM_INT);
            $stmt->execute();

            $sql = ("UPDATE {$dbprefix}users SET troops = :troops, recieved_troops = :recieved_troops WHERE user_id='$uid_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $troops = $uid_a[troops] + 10000;
            $recieved_troops = 1;

            $stmt->bindParam(':troops', $troops, PDO::PARAM_INT);
            $stmt->bindParam(':recieved_troops', $recieved_troops, PDO::PARAM_INT);
            $stmt->execute();

            $outcome_good[] = 'You have successfully sent 10,000 troops abroad.';
        }
    }
    if(isset($_POST['bombing_campaign'])) {
        if ($u_a[user_id] == $uid_a[user_id]) {
            $debug_info[] = 'Attempted suicide.';
            $errors[] = '"Never go to war. Especially with yourself." - Yuri Orlov';
        }
        if($u_a[air_force] < 1) {
            $errors[] = 'You must have an air force launch a bombing campaign.';
        }
        if($u_a[oil_reserves] < 5) {
            $errors[] = 'You must have at least 5 Mbbl\'s of oil to launch a bombing campaign.';
        }
        if($u_a[has_attacked] > 0) {
            $errors[] = 'You can only attack once per turn.';
        }
        if(!$errors) {
            $sql = ("UPDATE {$dbprefix}users SET growth = :growth, funds = :funds, gdp = :gdp WHERE user_id='$uid_a[user_id]'");
            $stmt = $pdo->prepare($sql);

            $air_force = $u_a[air_force] - $uid_a[air_force];

            if($air_force <= 0) {
                $outcome_bad[] = 'The opposing country\'s air force defends their country from the bombing campaign.';
                $growth = $uid_a[growth];
                $funds = $uid_a[funds];
                $gdp = $uid_a[gdp];
            }elseif($air_force <= 5) {
                $outcome_good[] = 'The bombing campaign has been successfully launched. The opponent has lost <b>5%</b> of their funds, growth, and GDP.';
                $growth = $uid_a[growth] * 0.95;
                $funds = $uid_a[funds] * 0.95;
                $gdp = $uid_a[gdp] * 0.95;
            } elseif($air_force > 5) {
                $outcome_good[] = 'The bombing campaign has been successfully launched. The opponent has lost <b>10%</b> of their funds, growth, and GDP.';
                $growth = $uid_a[growth] * 0.90;
                $funds = $uid_a[funds] * 0.90;
                $gdp = $uid_a[gdp] * 0.90;
            }

            $stmt->bindParam(':growth', $growth, PDO::PARAM_INT);
            $stmt->bindParam(':funds', $funds, PDO::PARAM_INT);
            $stmt->bindParam(':gdp', $gdp, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->rowCount() > 0) {

                $sql = ("UPDATE {$dbprefix}users SET has_attacked = :has_attacked WHERE user_id='$u_a[user_id]'");
                $stmt = $pdo->prepare($sql);

                // Config
                $has_attacked = true;
                $stmt->bindParam(':has_attacked', $has_attacked, PDO::PARAM_INT);
                $stmt->execute();

                if($stmt->rowCount() > 0) {
                    $sql = "INSERT INTO {$dbprefix}events (event_type,event_action,attacker_id,defender_id) VALUES (:event_type,:event_action,:attacker_id,:defender_id)";
                    $stmt = $pdo->prepare($sql);

                    // Config
                    $event_type = 'battle';
                    $event_action = 'launched a bombing campaign against';

                    $stmt->bindParam(':event_type', $event_type, PDO::PARAM_STR);
                    $stmt->bindParam(':event_action', $event_action, PDO::PARAM_STR);
                    $stmt->bindParam(':attacker_id', $u_a[user_id], PDO::PARAM_INT);
                    $stmt->bindParam(':defender_id', $uid_a[user_id], PDO::PARAM_INT);

                    $stmt->execute();
                }
            }
        }
    }
?>