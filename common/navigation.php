<?
    require_once("models/config.php");
    require_once("basicfunctions.php");
    require_once("external/analyticstracking.php");
    // require_once("external/uservoice.php");
    require_once("userdata.php");
    require_once("db/pdo.php");
    
    if($u_a[overwrite_theme] == true) {
        if($uid) {
            echo gettheme($uid_a[user_id]);
        } else {
            echo gettheme($u_a[user_id]);
        }
    } else {
        echo gettheme($u_a[user_id]);
    }
    // echo gettheme($u_a[user_id]);
?>

<html>

    <head>
        <title><? echo $websiteName; ?></title>
        <meta name="viewport" content="width=device-width">
        <link href="css/custom.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="https://netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery.foggy.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-maxlength.min.js"></script>
        <?
            if($u_a[icons_enabled] == true or !$u_a) {
                echo '<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.css" rel="stylesheet">';
            }
        ?>
    </head>
  
    <body>
        <img id="bg" src="
        <?
            $bginput = array(
                // "images/bg/1.jpg", 
                // "images/bg/2.jpg",
                // "images/bg/3.jpg",
                // "images/bg/4.jpg",
                // "images/bg/5.jpg",
                // "images/bg/6.jpg",
                // "images/bg/7.jpg",
                // "images/bg/8.jpg",
                // "images/bg/9.jpg",
                "images/bg/10.jpg",
                // "images/bg/11.jpg",
                // "images/bg/12.jpg",
                // "images/bg/13.jpg",
                // "images/bg/14.jpg",
                // "images/bg/15.jpg",
                // "images/bg/16.jpg",
                // "images/bg/17.jpg",

                // "images/bg/4.png",
                // "images/bg/5.png",
                ""
            );
                $rand_keys = array_rand($bginput, 2);
                echo $bginput[$rand_keys[0]] . "\n";
        ?>"/>

        <script type="text/javascript">
            // $('#bg').foggy({
            //     blurRadius: 2,          // In pixels.
            //     opacity: 0.2,           // Falls back to a filter for IE.
            //     cssFilterSupport: true  // Use "-webkit-filter" where available.
            // });
            $('#bg').fadeTo(0,0.2);
        </script>

        <div class="navbar navbar-static-top navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="<? if($_SERVER['SCRIPT_NAME'] == '/user.php') echo 'active'; ?>">
                            <a href="index">Attrition <sup>BETA</sup></a>
                        </li>
                        <? if(isUserLoggedIn()) { ?>
                            <li class="<? if($_SERVER['SCRIPT_NAME'] == '/inbox.php') echo 'active'; ?>">
                                <a href="inbox"><i class="fa fa-inbox"></i> Inbox
                                <?
                                    echo getmailcount($u_a[user_id]);
                                ?>
                                </a>
                            </li>
                            <li class="dropdown <? if($_SERVER['SCRIPT_NAME'] == '/policies.php') echo 'active'; ?>">
                                <a href="policies" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-briefcase"></i> Policies <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="policies?type=economic">Economic</a></li>
                                    <li><a href="policies?type=domestic">Domestic</a></li>
                                    <li><a href="policies?type=foreign">Foreign</a></li>
                                    <li><a href="policies?type=military">Military</a></li>
                                </ul>
                            </li>
                            <?
                                if($u_a[is_admin] == true) {
                                    echo '<li><a href="apanel"><i class="fa fa-cog"></i> Admin Panel</a></li>';
                                }
                            ?>
                        <? } ?>
                    </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown
                                <?
                                    if($_SERVER['SCRIPT_NAME'] == '/world.php' or
                                    $_SERVER['SCRIPT_NAME'] == '/alliances.php' or
                                    $_SERVER['SCRIPT_NAME'] == '/eventlog.php')
                                    echo 'active';
                                ?>
                            ">
                                <a href="world" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-globe"></i> The World <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="world">Global</a></li>
                                    <li><a href="world?region=africa">Africa</a></li>
                                    <li><a href="world?region=americas">Americas</a></li>
                                    <li><a href="world?region=asia">Asia</a></li>
                                    <li><a href="world?region=europe">Europe</a></li>
                                    <li><a href="world?region=mideast">Middle East</a></li>
                                    <li class="divider"></li>
                                    <li><a href="world?region=west">The West</a></li>
                                    <li><a href="world?region=east">The East</a></li>
                                    <li class="divider"></li>
                                    <li class="<? if($_SERVER['SCRIPT_NAME'] == '/alliances.php') echo 'active'; ?>">
                                      <a href="alliances"><i class="fa fa-sitemap"></i> Alliances</a>
                                    </li>
                                    <li class="<? if($_SERVER['SCRIPT_NAME'] == '/eventlog.php') echo 'active'; ?>">
                                      <a href="eventlog"><i class="fa fa-list"></i> Event Log</a>
                                    </li>
                                    <li class="<? if($_SERVER['SCRIPT_NAME'] == '/news.php') echo 'active'; ?>">
                                      <a href="news"><i class="fa fa-globe"></i> News Room</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="dropdown <? if($_SERVER['SCRIPT_NAME'] == '/irc.php') echo 'active'; ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-keyboard-o"></i> Community <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li>
                                      <a href="https://bitbucket.org/sven62/attrition/issues?status=new&status=open" target="_blank"><i class="fa fa-code"></i> Issue Tracker</a>
                                    </li>
                                    <li class="<? if($_SERVER['SCRIPT_NAME'] == '/irc.php') echo 'active'; ?>">
                                      <a href="irc"><i class="fa fa-comments"></i> IRC</a>
                                    </li>
                                    <li>
                                      <a href="http://facepunch.com/showthread.php?t=1329441" target="_blank"><i class="fa fa-keyboard-o"></i> Facepunch Forums</a>
                                    </li>
                                </ul>
                            </li>

                        <? if(!isUserLoggedIn()) { ?>
                            <li class="<? if($_SERVER['SCRIPT_NAME'] == '/register.php') echo 'active'; ?>">
                                <a href="register"><i class="fa fa-edit"></i> Register</a>
                            </li>
                            <li class="dropdown <? if($_SERVER['SCRIPT_NAME'] == '/login.php') echo 'active'; ?>">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sign-in"></i> Login <b class="caret"></b></a>
                                    <ul class="dropdown-menu" style="padding: 15px;min-width: 250px;">
                                        <li>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                                        <div class="form-group">
                                                           <label class="sr-only" for="username">Username</label>
                                                           <input type="text" class="form-control" name="username" id="username" placeholder="Username" maxlength="20">
                                                        </div>
                                                        <div class="form-group">
                                                           <label class="sr-only" for="password">Password</label>
                                                           <input type="password" class="form-control" name="password" id="password" placeholder="Password" maxlength="50">
                                                        </div>
                                                        <div class="checkbox">
                                                           <label>
                                                           <input type="checkbox"> Remember me
                                                           </label>
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-primary btn-block" name="new" id="newfeedform" value="Login">Login</button>
                                                        </div>
                                                        <center><a href="forgot-password">Forgot Password?</a></center>
                                                    </form>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                            </li>
                        <? } else { ?>
                            <li class="<? if($_SERVER['SCRIPT_NAME'] == '/preferences.php') echo 'active'; ?>">
                                <a href="preferences"><i class="fa fa-cog"></i> Preferences</a>
                            </li>
                            <li class="<? if($_SERVER['SCRIPT_NAME'] == '/help.php') echo 'active'; ?>">
                                <a href="help"><i class="fa fa-question"></i> Help</a>
                            </li>
                            <li class="<? if($_SERVER['SCRIPT_NAME'] == '/donate.php') echo 'active'; ?>">
                              <a href="donate"><i class="fa fa-money"></i> Donate</a>
                            </li>
                            <li>
                                <a href="logout"><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        <? } ?>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>