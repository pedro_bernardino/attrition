<?php
	set_include_path('/home/fridaypl/public_html/attrition/');
	require_once("common/db/link_mysql.php");
	require_once("common/db/pdo.php");

	$result = mysql_query("SELECT * FROM {$dbprefix}users WHERE active='1'", $link);

	$num_rows = mysql_num_rows($result);
  	$row_crm = mysql_fetch_assoc($result);

    // Update economy
    $sql = ("UPDATE {$dbprefix}economy SET var1 = :var1 WHERE commodity='oil'");
    $stmt = $pdo->prepare($sql);

    $var1 = mt_rand(24000, 26000);

    $stmt->bindParam(':var1', $var1, PDO::PARAM_INT);
    $stmt->execute();

	do {
		// Debug
		echo '<p>' . $row_crm[username] . '<br>Current GDP: $' . $english_format_number = number_format($row_crm[gdp]) . '<br>Growth: $' . $english_format_number = number_format($row_crm[growth]) . '<br>Result GDP: $' . $english_format_number = number_format($row_crm[gdp] + $row_crm[growth] ). ')</p>';

	    // Update Stats
	    $sql = ("UPDATE {$dbprefix}users SET qol = :qol, gdp = :gdp, growth = :growth, reputation = :reputation, manpower = :manpower, oil_reserves = :oil_reserves, has_attacked = :has_attacked, recieved_troops = :recieved_troops, has_posted_news = :has_posted_news WHERE user_id='$row_crm[user_id]'");
	    $stmt = $pdo->prepare($sql);

	    // Growth
	    if($row_crm[growth] >= 20000000 and $row_crm[growth] <= 39000000) {
	    	$growth = $row_crm[growth] - 1000000;
	    } elseif($row_crm[growth] >= 40000000 and $row_crm[growth] <= 59000000) {
	    	$growth = $row_crm[growth] - 2000000;
	    } elseif($row_crm[growth] >= 60000000 and $row_crm[growth] <= 79000000) {
	    	$growth = $row_crm[growth] - 4000000;
	    } elseif($row_crm[growth] >= 80000000 and $row_crm[growth] <= 99000000) {
	    	$growth = $row_crm[growth] - 8000000;
	    } elseif($row_crm[growth] >= 100000000) {
	    	$growth = $row_crm[growth] - 16000000;
	    } else {
	    	$growth = $row_crm[growth];
	    }
		// GDP
	    // If quality of life is greater than or equal to five, always give gdp.
			if($row_crm[qol] >= 5) {
			    $gdp = $row_crm[gdp] + $row_crm[growth];
				$outcome_string = 'Your country has sustained another month and experienced growth.';
			} elseif($row_crm[qol] <= 4 ) {
				$outcome = rand(1, 3);

				if($outcome == 3) {
					$gdp = $row_crm[gdp];
					$outcome_string = 'Your country has sustained another month and experienced growth.';
				} else {
					$gdp = $row_crm[gdp];
					$outcome_string = 'Your country has not experienced growth due to poor quality of life.';
				}
			}
		// Training
	    // if($row_crm[training] > 0 ) {
	    // 	$training = $row_crm[training] - 1;
	    // } elseif($row_crm[training] <= 0 ) {
	    // 	$training = $row_crm[training];
	    // }
		// Manpower
	    if($row_crm[manpower] <= 4) {
	    	$manpower = $row_crm[manpower] + 1;
	    } elseif($row_crm[manpower] >= 5 and $row_crm[manpower] <= 7) {
	    	$manpower = $row_crm[manpower] + 2;
	    } elseif($row_crm[manpower] >= 7 ) {
	    	$manpower = 10;
	    }
		// Oil
	    $oil_reserves = $row_crm[oil_reserves] + $row_crm[oil_production];
	    // Regen
	    if($row_crm[qol] < 10) {
	    	$qol = $row_crm[qol] + 1;
	    }
	    if($row_crm[reputation] < 10) {
	    	$reputation = $row_crm[reputation] + 1;
	    }
		// Reset after turn
	    $has_attacked = false;
	    $recieved_troops = false;
	    $has_posted_news = 0;

		$stmt->bindParam(':has_posted_news', $has_posted_news, PDO::PARAM_INT);
	    $stmt->bindParam(':growth', $growth, PDO::PARAM_INT);
	    $stmt->bindParam(':gdp', $gdp, PDO::PARAM_INT);
	    // $stmt->bindParam(':training', $training, PDO::PARAM_INT);
	   	$stmt->bindParam(':manpower', $manpower, PDO::PARAM_INT);
	    $stmt->bindParam(':qol', $qol, PDO::PARAM_INT);
	    $stmt->bindParam(':reputation', $reputation, PDO::PARAM_INT);
	    $stmt->bindParam(':oil_reserves', $oil_reserves, PDO::PARAM_INT);
	    $stmt->bindParam(':has_attacked', $has_attacked, PDO::PARAM_INT);
	    $stmt->bindParam(':recieved_troops', $recieved_troops, PDO::PARAM_INT);
	    $stmt->execute();

	    // Continue without errors
	    if(count($errors) == 0) {

	      	// Mail Query
	      	$sql = ("INSERT INTO {$dbprefix}mail (user_id, mail_type, title) VALUES (:user_id, :mail_type, :title)");
	      	$stmt = $pdo->prepare($sql);

	      	// Config
	      	$mail_type = 'event';

	    	// Bind
	    	$stmt->bindParam(':user_id', $row_crm[user_id], PDO::PARAM_INT);
	    	$stmt->bindParam(':mail_type', $mail_type, PDO::PARAM_STR); 
	    	$stmt->bindParam(':title', $outcome_string, PDO::PARAM_STR);

	    	$stmt->execute();
	    }
	} while ($row_crm = mysql_fetch_assoc($result));
?>