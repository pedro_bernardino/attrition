<?
    require_once("models/config.php");
    require_once("common/basicfunctions.php");
    require_once("common/userdata.php");
?>
<html>
	<head>
		<title>Dashboard | <? require_once("models/config.php"); echo $websiteName; ?></title>
	</head>

	<body>
		<? require_once("common/navigation.php"); ?>

	    <div class="container">
	    	<div class="well">
	    		<? require_once("common/alerts.php"); ?>

	    		<div class="row">
	    			<div class="col-md-6">

<input type="submit" class="btn btn-primary btn-block" name="new" id="newfeedform" value="Register" />
<!-- <div class="trigger">Trigger</div> -->
<div class="result"></div>
<div class="log"></div>


<script type="text/javascript">
	$( document ).ajaxSend(function() {
	  $( ".log" ).text( "Triggered ajaxSend handler." );
	});

	$( ".btn" ).submit(function( event ) {
	  $( ".result" ).load( "ajax/test.html" );
	});

	$(document).keypress(function(e) {
	    if(e.which == 13) {
	        $( ".log" ).text( "Triggered ajaxSend handler." );
	    }
	});
</script>

	    			</div>
	    			<div class="col-md-6" align="right">

	    			</div>
	    		</div>
		    	<? require_once("common/footer.php"); ?>
		    </div>
	    </div>
	</body>
</html>