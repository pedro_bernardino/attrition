<?
	require_once("models/config.php");
	require_once("common/userdata.php");

	if(!isUserLoggedIn()) { 
		include('landing.php'); 	
	} else {
		ob_start();
		header('Location: user.php?uid='.$u_a[user_id]); 	 
	}
?>