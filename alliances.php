<?php
	require_once("models/config.php");
	require_once("common/db/pdo.php");
	require_once("common/uiddata.php");
	require_once("common/userdata.php");
  $aid = $u_a[alliance_id];
	require_once("common/alliancedata.php");
	require_once("common/alliancefunctions.php");
?>
<html>
    <head>
        <title>Alliances | <?php echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <? require_once("common/alerts.php"); ?>

        <?php

          // Require again to get updated values
          require("common/userdata.php");
          $aid = $u_a[alliance_id];
          require("common/alliancedata.php");

          $tab = $_GET['tab'];

          if($tab == "")
          {
            $tab = "list";
          } if ($tab == "preferences") {
            require_once("common/alliances/preferences.php");
          } if ($tab == "list") {
            require_once("common/alliances/list.php");
          } if ($tab == "profile") {
            require_once("common/alliances/profile.php");
          } if ($tab == "create") {
            require_once("common/alliances/create.php");
          } if ($tab == "confirm") {
            require_once("common/alliances/confirm.php");
          } if ($tab == "leave") {
            require_once("common/alliances/leave.php");
          }
        ?>
        
                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>