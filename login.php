<?
	ob_start();
	require_once("models/config.php");
	require_once("common/basicfunctions.php");
  	if(isUserLoggedIn()) { header("Location: dashboard.php"); die(); }

	if(!empty($_POST)) {
		$errors = array();
		$username = trim($_POST["username"]);
		$password = trim($_POST["password"]);
		$remember_choice = true;
	
		//Perform some validation
		//Feel free to edit / change as required
		if($username == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_USERNAME");
		}
		if($password == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
		}
		echo bannedcheck($username);

		//End data validation
		if(count($errors) == 0)
		{
			//A security note here, never tell the user which credential was incorrect
			if(!usernameExists($username))
			{
				$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
			}
			else
			{
				$userdetails = fetchUserDetails($username);
			
				//See if the user's account is activation
				if($userdetails["active"]==0)
				{
					$errors[] = lang("ACCOUNT_INACTIVE");
				}
				else
				{
					//Hash the password and use the salt from the database to compare the password.
					$entered_pass = generateHash($password,$userdetails["password"]);

					if($entered_pass != $userdetails["password"])
					{
						//Again, we know the password is at fault here, but lets not give away the combination incase of someone bruteforcing
						$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
					}
					else
					{
						//passwords match! we're good to go'
						
						//Construct a new logged in user object
						//Transfer some db data to the session object
						$loggedInUser = new loggedInUser();
						$loggedInUser->email = $userdetails["email"];
						$loggedInUser->user_id = $userdetails["user_id"];
						$loggedInUser->hash_pw = $userdetails["password"];
						$loggedInUser->display_username = $userdetails["username"];
						$loggedInUser->uid = $userdetails["username"];
						$loggedInUser->clean_username = $userdetails["username_clean"];

						$loggedInUser->remember_me = $remember_choice;
						$loggedInUser->remember_me_sessid = generateHash(uniqid(rand(), true));
						
						//Update last sign in
						$loggedInUser->updatelast_sign_in();
		
						if($loggedInUser->remember_me == 0)
$_SESSION["userPieUser"] = $loggedInUser;
else if($loggedInUser->remember_me == 1) {
$db->sql_query("INSERT INTO ".$db_table_prefix."sessions VALUES('".time()."', '".serialize($loggedInUser)."', '".$loggedInUser->remember_me_sessid."')");
setcookie("userPieUser", $loggedInUser->remember_me_sessid, time()+parseLength($remember_me_length));
}
						
						//Redirect to user account page
						ob_start();
						header("Location: donate.php");
						die();
					}
				}
			}
		}
	}
?>

<html>
	<head>
	  <title>Login | <? echo $websiteName; ?></title>
	</head>

	<body>
		<? require_once("common/navigation.php"); ?>

	  	<!-- Content -->
	  	<div class="container">
	    	<div class="well">
	  			<?
	  				require_once("common/alerts.php");

		        	if(($_GET['status']) == "success") {
			        	// echo "<p>Your account was created successfully. Please login.</p>";
			        	echo alert(success,'Your account was created successfully. Please login.');
		    		}
		    	?>
		    	<div class="row">
		    		<div class="col-md-12">
			    		<div class="panel">
		    				<div class="panel-heading">Login</div>
			    			<div class="panel-body">
								<form name="newUser" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
									<p>
									    <input type="text"  name="username" class="form-control" placeholder="Username" maxlength="25"/>
									</p>
									<p>
									     <input type="password" name="password" class="form-control" placeholder="Password" length="50"/>
									</p>
							</div>
				    		<div class="panel-footer">
				    			<input type="submit" class="btn btn-primary btn-block" name="new" id="newfeedform" value="Login" />
				    			</form>
				    		</div>
				    	</div>
					</div>
				</div>
				<? require_once("common/footer.php"); ?>
    		</div>
  		</div>
	</body>
</html>