<?php
    require_once("models/config.php");
?>
<html>
    <head>
        <title>Event Log | <?php echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <?
                    require_once("common/alerts.php");
                ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-body">

                                <?php

                                    $limit = 50;

                                    if(isset($_GET["page"]) and $_GET["page"] == 0 ) {

                                        $page  = 1;

                                    } elseif(isset($_GET["page"])) {

                                        $page  = $_GET["page"];

                                    } else {

                                        $page=1;

                                    };  

                                    $start_from = ($page-1) * $limit;  

                                    // Retrieve data
                                    $wars_result = mysql_query("SELECT * FROM attr_events ORDER BY event_id DESC LIMIT $start_from, $limit", $link);

                                    $row = mysql_fetch_assoc($wars_result);
                                    $num_rows = mysql_num_rows($wars_result);

                                    // Display data
                                    if($num_rows > 0) { ?>
                                    
                                    <table class="table table-hover table-striped">
                                            <tbody>

                                            <? do { 

                                                    $id = $row[attacker_id];
                                                    $debug_info[] = 'Attacker ID: ' . $row[attacker_id];
                                                    $result = mysql_query("SELECT * FROM attr_users WHERE user_id='{$row[attacker_id]}'", $link);
                                                    if (!$result) {
                                                          die('Could not query:' . mysql_error());
                                                    } else {
                                                          $attacker = mysql_fetch_array($result);
                                                    }
                                                    $id = $row[defender_id];
                                                    $debug_info[] = 'Defender ID: '. $row[defender_id];
                                                    $result = mysql_query("SELECT * FROM attr_users WHERE user_id='{$row[defender_id]}'", $link);
                                                    if (!$result) {
                                                          die('Could not query:' . mysql_error());
                                                    } else {
                                                          $defender = mysql_fetch_array($result);
                                                    }

                                                ?>

                                                <tr>
                                                    <td>
                                                        <small class="text-muted"><? echo $row[event_id] ?></small>
                                                    </td>
                                                    <td>
                                                        <small><a href="user.php?uid=<? echo $attacker[user_id]; ?>"><? echo stripcslashes(ucwords($attacker[country_name])) ?></a></small>

                                                        <small class="text-muted"><? echo ucfirst($row[event_action]) ?></small>

                                                        <small><a href="user.php?uid=<? echo $defender[user_id]; ?>"><? echo stripcslashes(ucwords($defender[country_name])) ?></a></small>
                                                    </td>
                                                    <td>
                                                        <small class="text-muted"><? echo date('m/d/y', strtotime($row['event_date'])).' at '.date('g:i A T',strtotime($row[event_date])) ?></small>
                                                    </td>
                                                </tr>

                                            <? } while ($row = mysql_fetch_assoc($wars_result));

                                        }

                                        ?>

                                        </tbody>
                                    </table>

                                <center>
                                    <?php  

                                        $result = mysql_query("SELECT COUNT(*) FROM attr_events WHERE event_type='war'", $link);
                                        $row = mysql_fetch_row($result);

                                        $total_records = $row[0];  
                                        $total_pages = ceil($total_records / $limit); 
                                        $pagLink = '<ul class="pagination">';

                                        for ($i=1; $i<=$total_pages; $i++) {  
                                                     $pagLink .= "<li><a href='eventlog.php?page=".$i."''>".$i."</a></li>";  
                                        };

                                        echo $pagLink . '</ul>';  
                                    ?>
                                </center>

                            </div>
                        </div>
                    </div>
                </div> 
                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>