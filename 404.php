<html>
    <? require_once("models/config.php"); ?>
    <head>
        <title>404 | <? echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-md-6" align="left">
                        <h1 class="text-danger" class="page-header">
                            Error: 404: Not Found.
                        </h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" align="left">
                        <h6 class="text-danger"/>The requested resource could not be found but may be available again in the future.</h6>
                    </div>
                    <div class="col-md-6" align="right">
                        <h6 class="text-danger">If you think this is the result of a bug, please use the <b><a href="https://bitbucket.org/sven62/attrition/issues">issue tracker</a></b> to report it.</h6>
                    </div>
                </div>
            <? require_once("common/footer.php"); ?>
        </div>
    </body>
</html>