<? 
    require_once("common/navigation.php");
    require_once("common/db/pdo.php");

	if($u_a[is_admin] == false) {
		echo '
		<center><h1 class="page-header h1-landing">YOU\'RE NOT AN ADMIN!</h1>
		<h1 class="h3-landing">LOGIN TO AN ACCOUNT WITH ADMIN PRIVILEGES TO CONTINUE.</h1></center>
		';
		die();
	}

	if (!empty($_POST['query_user'])) {
        // Validate
       	$user = trim($_POST["query_user"]);
       	$user = filter_var($user, FILTER_SANITIZE_URL);

        if($u_a[is_admin] == false) {
        	$errors[] = 'You must be an administrator to use this function.';
        }
        if(!$user) {
        	$errors[] = 'You must enter a uid or username.';
        }
        if(!$errors) {
	        $sql = ("SELECT * FROM {$dbprefix}users WHERE user_id='$user' or username='$user'");
	        $stmt = $pdo->query($sql);

			while($result = $stmt->fetch()){
				$query_result = $result;
			}
        }

	}

	if($u_a[is_admin] == true) {
?>

<html>
    <head>
        <title>Admin Panel</title>
    </head>
    <body>
        <div class="container">
			<div class="well">
				<? require_once("common/alerts.php"); ?>
				<div class="row">
					<div class="col-md-6">
						<h3>Functions</h3>
					</div>
					<div class="col-md-6" align="right">
						<h3>Results</h3>
					</div>
				</div>

				<hr>
				<div class="row">
					<div class="col-md-6">
						<h5>Query User Info</h5>
						<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
							<input type="text" class="form-control" placeholder="UID or Username" name="query_user" maxlength="20">
						</form>
					</div>
					<div class="col-md-6">
						<h5>Results</h5>
						<pre><small>
						<? print_r($query_result); ?>
						</small></pre>
					</div>
				</div>

		    <? require_once("common/footer.php"); ?>
		    </div>
	    </div>
	</body>
<? } ?>