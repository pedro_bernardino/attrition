<?php
    require_once("models/config.php");
    require_once("common/basicfunctions.php");
    require_once("common/db/link_mysql.php");
?>
<html>
    <head>
        <title>The World | <?php echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <?
                    require_once("common/alerts.php");

                    $orderby = mysql_real_escape_string($_GET['sort']);
                    if ($orderby == '') { $orderby = 'gdp DESC'; }

                    if ($orderby == 'uid') {
                        $orderby = 'user_id ASC';
                    } elseif($orderby == 'usernane') {
                        $orderby = 'usernane ASC';
                    } elseif($orderby == 'country_nane') {
                        $orderby = 'country_nane ASC';
                    } elseif($orderby == 'alignment') {
                        $orderby = 'alignment ASC';
                    } elseif($orderby == 'region') {
                        $orderby = 'region ASC';
                    } elseif($orderby == 'gdp') {
                        $orderby = 'gdp DESC';
                    } elseif($orderby == 'troops') {
                        $orderby = 'troops DESC';
                    }
            
                    $region = mysql_real_escape_string($_GET['region']);
                    if ($region == "") { $region = "global"; }

                    if ($region == "mideast") {
                        $whereclause = " region='middle east' AND ";
                    } else if ($region == 'west') {
                        $whereclause = " alignment='2' AND ";
                    } else if ($region == 'east') {
                        $whereclause = " alignment='-2' AND ";
                    } else if ($region != "global") {
                        $whereclause = " region='{$region}' AND ";
                    } else {
                        $whereclause = "";
                    }
                ?>

                <div class="row">
                    <? require_once("common/worldsidebar.php") ?>
                    <div class="col-md-9">
                        <div class="panel panel-primary">
                            <div class="panel-body">

                                <?php

                                    $limit = 30;

                                    if(isset($_GET["page"]) and $_GET["page"] == 0 ) {

                                        $page  = 1;

                                    } elseif(isset($_GET["page"])) {

                                        $page  = $_GET["page"];

                                    } else {

                                        $page=1;

                                    };  

                                    $start_from = ($page-1) * $limit;  

                                    // Retrieve data
                                    $result = mysql_query("SELECT * FROM attr_users WHERE {$whereclause} active='1' ORDER BY {$orderby} LIMIT $start_from, $limit", $link);

                                    $row = mysql_fetch_assoc($result);
                                    $num_rows = mysql_num_rows($result);

                                    // Display data
                                    if($num_rows > 0) { ?>

                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th class="text-muted" style="width: 100%;">
                                                <h5 class="text-muted">Flag</h5></th>
                                                <th><h5 class="text-muted"> UID</h5></th>
                                                <th><h5 class="text-muted">Username</h5></th>
                                                <th><h5 class="text-muted">Country</h5></th>
                                                <th><h5 class="text-muted">Alignment</h5></th>
                                                <th><h5 class="text-muted">Region</h5></th>
                                                <th><h5 class="text-muted">Alliance</h5></th>
                                                <th><h5 class="text-muted">GDP</h5></th>
                                                <th><h5 class="text-muted">Troops</h5></th>
                                                <!-- <th><h5>Last Active</h5></th> -->
                                            </tr>
                                        </thead>

                                        <tbody>

                                        <? do { 
                                            if($row[alliance_id]) {
                                                $result2 = mysql_query("SELECT * FROM {$dbprefix}alliances WHERE alliance_id='$row[alliance_id]'", $link);
                                                $a_a = mysql_fetch_array($result2, MYSQL_BOTH);
                                            }

                                            ?>

                                            <tr>
                                                <td>
                                                    <?
                                                        if($row[custom_flag] and $u_a[safe_mode] == 0) {
                                                            echo '<a href="user.php?uid='.$row[user_id].'"><img style="width: 100%;" src="'.$row[custom_flag].'"></a>';
                                                        } else {
                                                            echo getflagfile($row[country_flag]);
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <h6><a href="user.php?uid=<? echo $row[user_id]; ?>" class="text-muted"><? echo $row[user_id]; ?></a></h6>
                                                </td>
                                                <td>
                                                    <h6>
                                                    <?
                                                        echo '<a href="user.php?uid='.$row[user_id].'">';
                                                        if($row[user_id] == $a_a[founder_id]) {
                                                            echo '<span class="text-warning"><i class="fa fa-star"></i> ';
                                                        }
                                                        if($row[donor] == true) {
                                                            echo '<span class="text-success"><i class="fa fa-heart"></i> ';
                                                        }
                                                        echo stripcslashes(ucfirst($row[username])).'</a></span>';
                                                    ?>
                                                    </h6>
                                                </td>
                                                <td>
                                                    <h6>
                                                    <?
                                                        $warinfo_query = mysql_query("SELECT * FROM {$dbprefix}events WHERE (attacker_id='$row[user_id]' OR defender_id='$row[user_id]') AND (event_type='war') AND (at_war='1') LIMIT 1", $link);
                                                        $warinfo = mysql_fetch_array($warinfo_query, MYSQL_BOTH);
                                                        $warinfo_rows = mysql_num_rows($warinfo_query);
                                                        if($warinfo_rows > 0 and $warinfo[at_war] == '1') {
                                                            echo '<b><a href="user.php?uid='.$row[user_id].'" class="text-danger">'.stripcslashes(ucwords($row[country_name])).'</a></b>';
                                                        } else {
                                                            echo '<a href="user.php?uid='.$row[user_id].'">'.stripcslashes(ucwords($row[country_name])).'</a>';
                                                        }
                                                    ?>
                                                    </h6>
                                                </td>
                                                <td>
                                                    <h6 class="text-muted"><? echo getalignment($row[user_id]); ?></h6>
                                                </td>
                                                <td>
                                                    <h6 class="text-success"><a href="world.php?region=<? echo $row[region]; ?>"><? echo ucwords($row[region]); ?></a></h6>
                                                </td>
                                                <td>
                                                    <?
                                                        if($row[alliance_id]) {
                                                            echo '<h6><a href="alliance.php?aid='.$a_a[alliance_id].'">'.stripcslashes(ucwords($a_a[alliance_name])).'</a></h6>';
                                                        } else {
                                                            echo '<h6 class="text-muted">Not Affiliated</h6>';
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <h6 class="text-success"><? echo '$'.number_format($row[gdp]) ?></h6>
                                                </td>
                                                <td>
                                                    <h6 class="text-danger"><? echo number_format($row[troops]) ?></h6>
                                                </td>
<!--                                                 <td>
                                                    <h6 class="text-muted"><? echo date('m/d/y',$row[last_sign_in]) ?></h6>
                                                </td> -->
                                            </tr>

                                        <? } while ($row = mysql_fetch_assoc($result));

                                    }

                                    ?>

                                    </tbody>
                                </table>
                                <center>
                                <?php  

                                    $result = mysql_query("SELECT COUNT(*) FROM {$dbprefix}users WHERE {$whereclause} active='1'", $link);
                                    $row = mysql_fetch_row($result);

                                    $total_records = $row[0];  
                                    $total_pages = ceil($total_records / $limit); 
                                    $pagLink = '<ul class="pagination">';

                                    for ($i=1; $i<=$total_pages; $i++) {  
                                                 $pagLink .= "<li><a href='world.php?region=".$region."&page=".$i."''>".$i."</a></li>";  
                                    };

                                    echo $pagLink . '</ul>';  
                                ?>
                                </center>
                            </div>
                        </div>
                    </div>
                </div> 
                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>