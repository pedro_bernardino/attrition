<?php
    require_once("models/config.php");
    require_once("common/basicfunctions.php");
    require_once("common/userdata.php");
    require_once("common/policyfunctions.php");

    $type = $_GET['type'];

    if(!isUserLoggedIn()) {
        header("Location: index.php"); die();
    }
?>

<html>
    <head>
        <title>Policies | <?php echo $websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <?
                    require_once("common/alerts.php");

                    // Require again to update values.
                    require("common/userdata.php");

                    if($type == "")
                    {
                      $type = "economic";
                    }

                    if($type == "economic")
                    {
                      require_once("common/policies/economic.php");
                    }

                    if($type == "domestic")
                    {
                      require_once("common/policies/domestic.php");
                    }

                    if($type == "foreign")
                    {
                      require_once("common/policies/foreign.php");
                    }

                    if($type == "military")
                    {
                      require_once("common/policies/military.php");
                    }              
                ?>

                <? require_once("common/footer.php"); ?>
            </div>
        </div>
    </body>
</html>