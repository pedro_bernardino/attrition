<?php
    require_once("models/config.php");
    require_once("common/db/link_mysql.php");
    require_once("common/basicfunctions.php");
    require_once("common/alliancefunctions.php");
    require_once("common/userdata.php");

    $aid = mysql_real_escape_string($_GET["aid"]);
    require_once("common/alliancedata.php");

    if($a_a[alliance_id] == null) {
        $result = mysql_query("SELECT alliance_id FROM {$dbprefix}alliances WHERE alliance_id='$aid' LIMIT 1", $link);
        if (!$result) {
            die('Could not query:' . mysql_error());
        } else {
            $result = mysql_query("SELECT alliance_id FROM {$dbprefix}alliances ORDER BY RAND() LIMIT 1", $link);
            $aid = mysql_result($result, 0);
            require("common/alliancedata.php");
            $errors[] = '<p class="lead">A player or country with that id was not found. A random profile has been chosen.</p>';
        }
    }

?>
<html>
    <head>
        <title><? echo $a_a[alliance_name].' | '.$websiteName; ?></title>
    </head>

    <body>
        <? require_once("common/navigation.php"); ?>

        <div class="container">
            <div class="well">
                <? require_once("common/alerts.php"); ?>

        <div class="row">

          <div class="col-md-6" align="left">

            <h2>
            <?php echo ucfirst($a_a[alliance_name]); ?>
            </h2>

          </div>

          <div class="col-md-6" align="right">

            <h5>
            <?
              $result = mysql_query("SELECT user_id, username, country_name, gov_type FROM {$dbprefix}users WHERE user_id='$a_a[founder_id]'", $link);
              $founder = mysql_fetch_array($result, MYSQL_BOTH);
              echo 'Founded by: <a href="user.php?uid=' . $founder[user_id] . '">' . getcountryprefix($founder[gov_type]) . ' <u>' . ucfirst($founder[country_name]) . '</u></a></u>';
            ?>
            </h5>

          </div>

        </div>

    <hr>

        <div class="row">
          	<div class="col-md-4">

              	<!-- Alliance Info -->
              	<div class="panel panel-default">
                	<div class="panel-heading">
                  		<h3 class="panel-title"><i class="fa fa-info-circle"></i> Alliance Information</h3>
                	</div>
                <div class="panel-body">

	                <small>
	                  <p class="text-muted">
	                  Alliance ID (UID): <?php echo ucfirst($a_a[alliance_id]); ?>
	                  </p>
	                  <p class="text-muted">
	                  Alliance Name: <?php echo '<a href="alliance.php?aid=' . $a_a[alliance_id] . '">' . ucfirst($a_a[alliance_name]) . '</a>'; ?>
	                  </p>
	                  <p class="text-muted">
	                  Founded By: 
                    <?
                      echo '<a href="user.php?uid=' . $founder[user_id] . '">' . getcountryprefix($founder[gov_type]) . ' ' . ucfirst($founder[country_name]) . '</a>';
                    ?>
	                  </p>
	                  <p class="text-muted">
	                  Establishment Date: <?php echo date("F d, o",strtotime($a_a[est_date])); ?>
	                  </p>
	                </small>

                </div>
              </div>

	            <!-- Description -->
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                  <h3 class="panel-title"><i class="fa fa-file-text"></i> Description</h3>
	                </div>
	                <div class="panel-body">
	                  <small>
	                  <?php 
	                    if($a_a[description] == null) {
                        echo '<p class="text-muted">This alliance\'s leader has not set a description.</p>';
	                    } else {
	                      echo '<p class="text-muted">' . ucfirst($a_a[description]) . '</p>';
	                    }
	                  ?>
	                  </small>
	                </div>
	            </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-flag"></i> Alliance Anthem</h3>
                    </div>
                    <div class="panel-body">
                        <?
                            if($a_a[alliance_anthem]) {
                                echo '<center>'.getallianceanthem($a_a[alliance_id]).'</center>';
                            } elseif(!$a_a[alliance_anthem]) {
                                echo '<span class="text-muted"><small>This alliance has not set an official anthem.</small></span>';
                            }
                        ?>
                    </div>
                </div>
                <? if($u_a[alliance_id] == $a_a[alliance_id]) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-file-text"></i> Alliance Functions</h3>
                    </div>
                    <div class="panel-body">
                        <form action="<? echo $_SERVER[PHP_SELF].'?aid='.$a_a[alliance_id]; ?>" method="post">
                        <button class="btn btn-success btn-block" data-toggle="modal" data-target="#supply_troops">
                            Supply 10,000 Troops
                        </button>
                        <div class="modal fade" id="supply_troops" tabindex="-1" role="dialog" aria-labelledby="supply_troops" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="supply_troops">Confirm</h4>
                              </div>
                              <div class="modal-body">
                                <input type="submit" class="btn btn-success btn-block" value="Supply 10,000 Troops" name="supply_troops">
                              </div>
                            </div>
                          </div>
                        </div>
                        </form>
                    </div>
                </div>
                <? } ?>

            </div>

          	<div class="col-md-8">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title"><i class="fa fa-flag"></i> Alliance Flag</h3>
                </div>
                <div class="panel-body">
                  <center>
                        <?
                            if($a_a[custom_flag]) {
                                echo '<img style="width: 100%;" src="'.$a_a[custom_flag].'">';
                            } else {
                                echo getflagfile($a_a[alliance_flag]);
                            }
                        ?>
                  </center>
                </div>
              </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-flag"></i> Alliance Stats</h3>
                    </div>
                    <div class="panel-body">
                        <p class="text-muted">Troop Bank: 
                        <?
                            if($a_a[troop_bank] == 0) {
                                echo 'Empty';
                            } else {
                                echo number_format($a_a[troop_bank]);
                            }
                        ?>
                        </p>
                    </div>
                </div>

            </div>

          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-primary">
                 <div class="panel-body">

                                <?php

                                    $whereclause = "alliance_id='$a_a[alliance_id]'";

                                    $limit = 30;

                                    if(isset($_GET["page"]) and $_GET["page"] == 0 ) {

                                        $page  = 1;

                                    } elseif(isset($_GET["page"])) {

                                        $page  = $_GET["page"];

                                    } else {

                                        $page=1;

                                    };  

                                    $start_from = ($page-1) * $limit;  

                                    // Retrieve data
                                    $result = mysql_query("SELECT * FROM {$dbprefix}users WHERE {$whereclause} ORDER BY gdp DESC LIMIT $start_from, $limit", $link);

                                    $row = mysql_fetch_assoc($result);
                                    $num_rows = mysql_num_rows($result);

                                    // Display data
                                    if($num_rows > 0) { ?>

                                    <table class="table table-hover table-striped">

                                        <thead>
                                            <tr>
                                                <th style="width: 10%" class="text-muted"><h5>Flag</h5></th>
                                                <th class="text-muted"><h5>UID</h5></th>
                                                <th class="text-muted"><h5>Username</h5></th>
                                                <th class="text-muted"><h5>Country Name</h5></th>
                                                <th class="text-muted"><h5>Alignment</h5></th>
                                                <th class="text-muted"><h5>Region</h5></th>
                                                <th class="text-muted"><h5>GDP</h5></th>
                                                <th class="text-muted"><h5>Last Active</h5></th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                        <? do { ?>

                                            <tr>
                                                <td>
                                                    <?
                                                        if($row[custom_flag] and $u_a[safe_mode] == 0) {
                                                            echo '<a href="user.php?uid='.$row[user_id].'"><img style="width: 100%;" src="'.$row[custom_flag].'"></a>';
                                                        } else {
                                                            echo getflagfile($row[country_flag]);
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <h6><a href="user.php?uid=<? echo $row[user_id]; ?>" class="text-muted"><? echo $row[user_id]; ?></a></h6>
                                                </td>
                                                <td>
                                                    <h6><a href="user.php?uid=<? echo $row[user_id]; ?>">
                                                      <?
                                                        if($row[user_id] == $a_a[founder_id]) {
                                                          echo '<i class="fa fa-star"></i> '.stripcslashes(ucfirst($row[username]));
                                                        } else {
                                                          echo stripcslashes(ucfirst($row[username]));
                                                        }
                                                      ?>
                                                    </a></h6>
                                                </td>
                                                <td>
                                                    <h6 class="text-muted"><? echo stripcslashes(ucwords($row[country_name])); ?></h6>
                                                </td>
                                                <td>
                                                    <h6 class="text-muted"><? echo getalignment($row[user_id]); ?></h6>
                                                </td>
                                                <td>
                                                    <h6><a href="world.php?region=<? echo $row[region]; ?>"><? echo ucwords($row[region]); ?></a></h6>
                                                </td>
                                                <td>
                                                    <h6 class="text-success"><? echo '$'.number_format($row[gdp]) ?></h6>
                                                </td>
                                                <td>
                                                    <h6 class="text-muted"><? echo date('m/d/y',$row[last_sign_in]) ?></h6>
                                                </td>
                                            </tr>

                                        <? } while ($row = mysql_fetch_assoc($result));

                                    }

                                    ?>

                                    </tbody>
                                </table>

                                <center>

                                    <?php  

                                        $result = mysql_query("SELECT COUNT(*) FROM {$dbprefix}users WHERE {$whereclause}", $link);
                                        $row = mysql_fetch_row($result);

                                        $total_records = $row[0];  
                                        $total_pages = ceil($total_records / $limit); 
                                        $pagLink = '<ul class="pagination">';

                                        for ($i=1; $i<=$total_pages; $i++) {  
                                                     $pagLink .= "<li><a href='alliance.php?aid=".$aid."&page=".$i."''>".$i."</a></li>";  
                                        };

                                        echo $pagLink . '</ul>';  
                                    ?>
                                </center>

                            </div>
                        </div>
                    </div>

        </div>
        <? require_once("common/footer.php"); ?>
      </div>
  </body>
</html>