<?php

	ob_start();
  	require_once("models/config.php");
  	require_once("db/user/u_theme.php");

	/* 
		Below is a very simple example of how to process a login request.
		Some simple validation (ideally more is needed).
	*/

//Forms posted
if(!empty($_POST))
{
		$errors = array();
		$password = $_POST["password"];
		$password_new = $_POST["passwordc"];
		$password_confirm = $_POST["passwordcheck"];
	
		//Perform some validation
		//Feel free to edit / change as required

		if(!isUserLoggedIn()) {

			$errors[] = 'You must be logged in to change your password.';

		}
		
		if(trim($password) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
		}
		else if(trim($password_new) == "")
		{
			$errors[] = lang("ACCOUNT_SPECIFY_NEW_PASSWORD");
		}
		else if(minMaxRange(8,50,$password_new))
		{	
			$errors[] = lang("ACCOUNT_NEW_PASSWORD_LENGTH",array(8,50));
		}
		else if($password_new != $password_confirm)
		{
			$errors[] = lang("ACCOUNT_PASS_MISMATCH");
		}
		
		//End data validation
		if(count($errors) == 0)
		{
			//Confirm the hash's match before updating a users password
			$entered_pass = generateHash($password,$loggedInUser->hash_pw);
			
			//Also prevent updating if someone attempts to update with the same password
			$entered_pass_new = generateHash($password_new,$loggedInUser->hash_pw);
		
			if($entered_pass != $loggedInUser->hash_pw)
			{
				//No match
				$errors[] = lang("ACCOUNT_PASSWORD_INVALID");
			}
			else if($entered_pass_new == $loggedInUser->hash_pw)
			{
				//Don't update, this fool is trying to update with the same password ÃÂ¬ÃÂ¬
				$errors[] = lang("NOTHING_TO_UPDATE");
			}
			else
			{
				//This function will create the new hash and update the hash_pw property.
				$loggedInUser->updatePassword($password_new);
			}
		}
	}
?>
<head>
	<title>Update Password | <?php echo $websiteName; ?> </title>

</head>

<body>

  <?php require_once("navigation.php"); ?>
    <div id="navigation">

  <!-- Content -->
  <div class="container">
    <div class="well">

  <?php require_once("notifications.php"); ?>
    <div id="notifications">

		<?php

			if(!empty($_POST)) {
	
				if ($errors) {

					echo '<p><div class="alert alert-dismissable alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
					errorBlock($errors);
					echo '</div></p>';

				} else {

					echo '<p><div class="alert alert-dismissable alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>';
					echo lang("ACCOUNT_DETAILS_UPDATED");
					echo '</div></p>';

				}

			}

        ?>

        <?php if(isUserLoggedIn()) { ?>

	    <form name="changePass" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
	    
            <div class="row">

 				<div class="col-md-4" align="left">

	            	<input type="password" name="password" class="form-control" placeholder="Password"/>

	            </div>
	        
	       		<div class="col-md-4" align="right">

	            	<input type="password" name="passwordc" class="form-control" placeholder="New Password"/>

	            </div>
				<div class="col-md-4" align="right">

	            	<input type="password" name="passwordcheck" class="form-control" placeholder="Confirm New Password"/>

	            </div>

	        </div>

	    </div>

		<input type="submit" class="btn btn-primary btn-block" name="new" id="newfeedform" value="Change Password" />

		<?php } ?>

  <?php require_once("footer.php"); ?>
    <div id="footer">

    </div>
  </div>

</body>
</html>